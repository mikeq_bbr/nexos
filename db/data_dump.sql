-- DROP exsiting Database and re-create it
DROP DATABASE IF EXISTS Applications;
CREATE DATABASE Applications;
USE Applications;

-- Creating Tables
CREATE TABLE `Industry` (
    `IndustryCode` VARCHAR(12),
    `IndustryName` VARCHAR(20),
    PRIMARY KEY (`IndustryCode`)
)
CREATE TABLE `SubIndustry` (
    `IndustryCode` VARCHAR(12),
    `SubIndustryCode` VARCHAR(12),
    `IndustryName` VARCHAR(25),
    PRIMARY KEY (`IndustryCode`, `SubIndustryCode`)
)
CREATE TABLE `School` (
    `SchoolCode` INT NOT NULL AUTO_INCREMENT,
    `State` VARCHAR(15),
    `City` VARCHAR(25),
    PRIMARY KEY (`SchoolCode`)
)
CREATE TABLE `Class` (
    `ClassID` VARCHAR(15),
    `ClassLevel` INT,
    `DegreeID` VARCHAR(20),
    `SchoolCode` VARCHAR(10),
    PRIMARY KEY (`ClassID`, `ClassLevel`)
)
CREATE TABLE `Degree` (
    `DegreeID` VARCHAR(20),
    `SchoolCode` VARCHAR(10),
    `EdLevel` CHAR,
    PRIMARY KEY (`DegreeID`)
)
CREATE TABLE `Job` (
    `JobID` VARCHAR(20),
    `DegreeID` VARCHAR(20),
    `JobTitle` VARCHAR(20),
    `Salary` INT,
    `Summary` VARCHAR(500),
    `EntryLevelEd` CHAR,
    `IndustryCode` VARCHAR(12),
    `SubIndustryCode` VARCHAR(12)
    PRIMARY KEY (`JobID`)
)
Delimiter //

CREATE PROCEDURE GetSubIndustry (IN inSubIndustryCode VARCHAR(12))
    BEGIN
    SELECT *
    FROM Industry
    WHERE SubIndustryCode = InSubIndustryCode;
    END //

Delimiter ;
